Write-Host "RUN THIS SCRIPT AS SUDO USERS" -ForegroundColor RED

if (!([string]::IsNullOrEmpty($args[0])))
{
	Write-Host "Target IP:$args" -ForegroundColor RED
}
else
{
	Write-Host "Your command line is incorrect"
	Write-Host "Usage example: #red sudo ./nmapScans.sh 10.10.10.121" -ForegroundColor RED
	exit 1
}
$DATE = (Get-Date -Format "yy/MM/dd")
$filename=$args[0] + "_" + $DATE

Write-Host "Running a quick scan ..." -ForegroundColor RED
nmap -T4 -F $args[0] | Out-File -FilePath .\quick_nmapscan_$filename.txt
Write-Host "Results -> quick_nmapscan_$filename.txt" -ForegroundColor RED

exit 0
