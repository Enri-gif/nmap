#!/bin/bash

RED='\033[0;31m'
WHITE='\033[0;37m'
NC='\033[0m'
isRunning=True
scanningType="-sT"

echo -e "${RED}RUN THIS SCRIPT AS SUDO USERS${WHITE}"

if [ $# -eq 1 ]; then
	echo -e "Target ${RED}IP:$1${WHITE}"
else
	echo "Your command line is incorrect"
	echo -e "Usage example: ${RED} sudo ./nmapScans.sh 10.10.10.121 ${WHITE}"
	exit 1
fi

DATE=`date +%Y-%m-%d`
filename=$1_DATE

while [ $isRunning ]
do
	echo -e "${RED}Choose scanning type${WHITE}"
	echo "[1]-sS TCP syn port scan"
	echo "[2]-sT TCP connect port scan"
	echo "[3]-sA TCP ack port scan"
	echo "[4]-sU UDP port scan"
	echo "[5]-sN Null scan"
	echo "[6]-sF FIN scan"
	echo "[7]-sX Xmas scan"
#	echo "[8] -Pn something"

	read option
	if [[ $option==1 ]] then
		scanningType="-sS"
	elif [[ $option==2 ]] then
		scanningType="-sT"
	elif [[ $option==3 ]] then
		scanningType="-sA"
	elif [[ $option==4 ]] then
		scanningType="-sU"
	elif [[ $option==5 ]] then
		scanningType="-sN"
	elif [[ $option==6 ]] then
		scanningType="-sF"
	elif [[ $option==7 ]] then
		scanningType="-sX"
	#elif [[ $option==8 ]]then
	#	scanningType="Pn"
	else
		exit 0
	fi
	break
done

#echo "Save scan to file?"
#read save

#if [[ $save=="y" ]] then
echo "Running nmap ${scanningType} -T4 -vv ${1}"
	nmap $scanningType -T4 -vv $1 > nmapscan.txt
#else
#	echo "Running nmap ${scanningType} -T4 -vv ${1}"
#	nmap $scanningType -T4 -F -vv $1
#fi
#read pause

exit 0
